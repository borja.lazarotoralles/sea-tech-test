package config

import "os"

const (
	ENV_HOST      = "PGHOST"
	ENV_PORT      = "PGPORT"
	ENV_USERNAME  = "PGUSERNAME"
	ENV_PASSWORD  = "PGPASSWORD"
	ENV_DATABASE  = "PGDATABASE"
	ENV_SSLMODE   = "PGSSLMODE"
	ENV_GRPC_PORT = "GRPCPORT"
)

type Config struct {
	Database DatabaseConfig
	Server   ServerConfig
}

type DatabaseConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	Database string
	SSLMode  string
}

type ServerConfig struct {
	GRPCPort    string
	GatewayPort string
}

func New() Config {
	host := os.Getenv(ENV_HOST)
	if host == "" {
		host = "localhost"
	}
	port := os.Getenv(ENV_PORT)
	if port == "" {
		port = "5432"
	}
	username := os.Getenv(ENV_USERNAME)
	if username == "" {
		username = "postgres"
	}
	password := os.Getenv(ENV_PASSWORD) // leave blank as default
	database := os.Getenv(ENV_DATABASE)
	if database == "" {
		database = "port"
	}
	sslMode := os.Getenv(ENV_SSLMODE)
	if sslMode == "" {
		sslMode = "disable"
	}
	grpcPort := os.Getenv(ENV_GRPC_PORT)
	if grpcPort == "" {
		grpcPort = "6000"
	}
	return Config{
		Database: DatabaseConfig{
			Host:     host,
			Port:     port,
			Username: username,
			Password: password,
			Database: database,
			SSLMode:  sslMode,
		},
		Server: ServerConfig{
			GRPCPort: grpcPort,
		},
	}
}
