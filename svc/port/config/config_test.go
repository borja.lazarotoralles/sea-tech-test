package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thanhpk/randstr"
)

func TestNew(t *testing.T) {

	tts := []struct {
		name string
		prep func() (want Config)
	}{
		{
			name: "happy path: environment is blank",
			prep: func() Config {
				return Config{
					Database: DatabaseConfig{
						Host:     "localhost",
						Port:     "5432",
						Username: "postgres",
						Password: "",
						Database: "port",
						SSLMode:  "disable",
					},
					Server: ServerConfig{
						GRPCPort: "6000",
					},
				}
			},
		},
		{
			name: "happy path: environment is set",
			prep: func() Config {
				host := setRandomEnvironmentValue(ENV_HOST)
				port := setRandomEnvironmentValue(ENV_PORT)
				username := setRandomEnvironmentValue(ENV_USERNAME)
				password := setRandomEnvironmentValue(ENV_PASSWORD)
				database := setRandomEnvironmentValue(ENV_DATABASE)
				sslMode := setRandomEnvironmentValue(ENV_SSLMODE)
				grpcPort := setRandomEnvironmentValue(ENV_GRPC_PORT)
				return Config{
					Database: DatabaseConfig{
						Host:     host,
						Port:     port,
						Username: username,
						Password: password,
						Database: database,
						SSLMode:  sslMode,
					},
					Server: ServerConfig{
						GRPCPort: grpcPort,
					},
				}
			},
		},
	}
	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			want := tt.prep()
			got := New()
			assert.Equal(t, want, got)
		})
	}
}

func setRandomEnvironmentValue(envKey string) string {
	envValue := randstr.String(10)
	os.Setenv(envKey, envValue)
	return envValue
}
