package cmd

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"

	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcLogging "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	portpb "tech.test/m/proto/gen/go/ports/v1"
	"tech.test/m/svc/port/config"
	"tech.test/m/svc/port/pkg/repo/postgres"
	"tech.test/m/svc/port/pkg/server"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use: "port",
	Run: func(cmd *cobra.Command, args []string) {
		// Initialise logger
		logger := getLogger()
		// Initialise config
		config := config.New()
		// Initialise repository
		connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
			config.Database.Username,
			config.Database.Password,
			config.Database.Host,
			config.Database.Port,
			config.Database.Database,
			config.Database.SSLMode,
		)
		repo, closeDB, err := postgres.New(connectionString, config.Database.Database)
		if err != nil {
			logger.Fatalf("initialising repository: %s", err.Error())
		}
		// Defer database closure
		defer closeDB()
		// Initialise gRPC server
		s := server.New(logger, repo)
		// Add logger middleware
		opt := []grpc.ServerOption{
			grpc.UnaryInterceptor(grpcMiddleware.ChainUnaryServer(
				grpcLogging.UnaryServerInterceptor(logger),
			)),
		}
		grpcServerBack := grpc.NewServer(opt...)
		// Defer gRPC server closure
		defer func() {
			logger.Println("Shutting down ...")
			grpcServerBack.GracefulStop()
			logger.Println("Server gracefully stopped")
		}()
		// Register gRPC server
		portpb.RegisterPortDomainServiceServer(grpcServerBack, s)
		reflection.Register(grpcServerBack)
		// Start gRPC server
		grpcAddress := fmt.Sprintf(":%s", config.Server.GRPCPort)
		go func() {
			listenerBack, err := net.Listen("tcp", grpcAddress)
			if err != nil {
				logger.Fatalf("gRPC listener: %s", err.Error())
			}
			logger.Println("Serving gRPC server on: ", grpcAddress)
			if err := grpcServerBack.Serve(listenerBack); err != nil {
				logger.Fatalf("gRPC service: %s", err.Error())
			}
		}()
		logger.Println("Port service successfully started")
		// Graceful shutdown
		stop := make(chan os.Signal, 1)
		signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
		<-stop
	},
}

// Execute runs the root command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func getLogger() *logrus.Entry {
	l := logrus.New()
	l.SetReportCaller(true)
	l.SetFormatter(&logrus.JSONFormatter{})
	logger := l.WithFields(logrus.Fields{
		"service": "port",
	})
	return logger
}
