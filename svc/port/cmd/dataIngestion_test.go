package cmd

import (
	"context"
	"errors"
	"io"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	portpb "tech.test/m/proto/gen/go/ports/v1"
	mock "tech.test/m/proto/gen/go_mock"
)

func Test_decodeJSON(t *testing.T) {
	tts := []struct {
		name       string
		portClient func(t *testing.T) *mock.MockPortDomainServiceClient
		reader     io.Reader
		wantCount  int
		wantErr    bool
	}{
		{
			name:       "happy path - empty input",
			portClient: mock.MockPortDomainClientFactory(),
			reader:     strings.NewReader(`{}`),
			wantCount:  0,
			wantErr:    false,
		},
		{
			name: "happy path - 2 ports",
			portClient: mock.MockPortDomainClientFactory(
				func(mpdsc *mock.MockPortDomainServiceClient) *gomock.Call {
					return mpdsc.EXPECT().CreatePort(context.Background(), &portpb.CreatePortRequest{
						Port: &portpb.Port{
							Name:        "Ajman",
							City:        "Ajman",
							Country:     "United Arab Emirates",
							Alias:       []string{},
							Regions:     []string{},
							Coordinates: []float32{55.5136433, 25.4052165},
							Province:    "Ajman",
							Timezone:    "Asia/Dubai",
							Unlocs:      []string{"AEAJM"},
							Code:        "52000",
						},
					}).Return(&portpb.CreatePortResponse{}, nil).Times(1)
				},
				func(mpdsc *mock.MockPortDomainServiceClient) *gomock.Call {
					return mpdsc.EXPECT().CreatePort(context.Background(), &portpb.CreatePortRequest{
						Port: &portpb.Port{
							Name:        "Abu Dhabi",
							City:        "Abu Dhabi",
							Country:     "United Arab Emirates",
							Alias:       []string{},
							Regions:     []string{},
							Coordinates: []float32{54.37, 24.47},
							Province:    "Abu Z¸aby [Abu Dhabi]",
							Timezone:    "Asia/Dubai",
							Unlocs:      []string{"AEAUH"},
							Code:        "52001",
						},
					}).Return(&portpb.CreatePortResponse{}, nil).Times(1)
				},
			),
			reader: strings.NewReader(`
			{
				"AEAJM": {
					"name": "Ajman",
					"city": "Ajman",
					"country": "United Arab Emirates",
					"alias": [],
					"regions": [],
					"coordinates": [
						55.5136433,
						25.4052165
					],
					"province": "Ajman",
					"timezone": "Asia/Dubai",
					"unlocs": [
						"AEAJM"
					],
					"code": "52000"
				},
				"AEAUH": {
					"name": "Abu Dhabi",
					"coordinates": [
						54.37,
						24.47
					],
					"city": "Abu Dhabi",
					"province": "Abu Z¸aby [Abu Dhabi]",
					"country": "United Arab Emirates",
					"alias": [],
					"regions": [],
					"timezone": "Asia/Dubai",
					"unlocs": [
						"AEAUH"
					],
					"code": "52001"
				}
			}`),
			wantCount: 2,
			wantErr:   false,
		},
		{
			name:       "error - array instead of object",
			portClient: mock.MockPortDomainClientFactory(),
			reader:     strings.NewReader(`[]`),
			wantCount:  0,
			wantErr:    true,
		},
		{
			name: "error - service error",
			portClient: mock.MockPortDomainClientFactory(
				func(mpdsc *mock.MockPortDomainServiceClient) *gomock.Call {
					return mpdsc.EXPECT().CreatePort(gomock.Any(), gomock.Any()).Return(nil, errors.New("boom")).Times(1)
				},
			),
			reader:    strings.NewReader(`{"something":{}}`),
			wantCount: 0,
			wantErr:   true,
		},
	}
	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			count, err := decodeJSON(context.Background(), tt.portClient(t), tt.reader)
			if (err != nil) != tt.wantErr {
				t.Errorf("expectedError is %v but error is %v", tt.wantErr, err)
				t.Log(err)
			}
			assert.Equal(t, tt.wantCount, count)
		})
	}
}
