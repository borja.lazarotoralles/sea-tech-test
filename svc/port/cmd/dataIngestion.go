package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/hashicorp/go-multierror"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	portpb "tech.test/m/proto/gen/go/ports/v1"
	"tech.test/m/svc/port/config"
)

var filePath string

func init() {
	rootCmd.AddCommand(dataIngestion)
	dataIngestion.PersistentFlags().StringVarP(&filePath, "filePath", "f", "", "file path to JSON containing ports data")
}

var dataIngestion = &cobra.Command{
	Use:   "data-ingestion",
	Short: "Starts a data ingestion pipeline from the supplied JSON ports file",
	Args:  cobra.ExactArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		// Initialise logger
		logger := getLogger()
		// Initialise config
		config := config.New()
		// Initialise connection to port service
		address := fmt.Sprintf(":%s", config.Server.GRPCPort)
		dialOptions := []grpc.DialOption{
			grpc.WithTransportCredentials(insecure.NewCredentials()),
		}
		portConn, err := grpc.Dial(address, dialOptions...)
		if err != nil {
			return fmt.Errorf("connecting to port service: %s", err.Error())
		}
		portClient := portpb.NewPortDomainServiceClient(portConn)
		// Start data ingestion
		logger.Infof("starting data ingestion from: %s", filePath)
		ctx := context.Background()
		file, err := os.Open(filePath)
		if err != nil {
			return fmt.Errorf("opening JSON file: %s", filePath)
		}
		defer file.Close()
		count, err := decodeJSON(ctx, portClient, file)
		logger.Infof("created %d ports", count)
		return err
	},
}

type EncodedPort struct {
	Name        string    `json:"name"`
	City        string    `json:"city"`
	Country     string    `json:"country"`
	Alias       []string  `json:"alias"`
	Regions     []string  `json:"regions"`
	Coordinates []float32 `json:"coordinates"`
	Province    string    `json:"province"`
	Timezone    string    `json:"timezone"`
	Unlocs      []string  `json:"unlocs"`
	Code        string    `json:"code"`
}

func (ep *EncodedPort) toPb() *portpb.Port {
	return &portpb.Port{
		Name:        ep.Name,
		City:        ep.City,
		Country:     ep.Country,
		Alias:       ep.Alias,
		Regions:     ep.Regions,
		Coordinates: ep.Coordinates,
		Province:    ep.Province,
		Timezone:    ep.Timezone,
		Unlocs:      ep.Unlocs,
		Code:        ep.Code,
	}
}

func decodeJSON(ctx context.Context, portClient portpb.PortDomainServiceClient, r io.Reader) (count int, err error) {
	decoder := json.NewDecoder(r)
	decoder.DisallowUnknownFields()
	// read opening token '['
	t, err := decoder.Token()
	if err != nil {
		return 0, fmt.Errorf("getting initial token: %s", err.Error())
	}
	if t != json.Delim('{') {
		return 0, fmt.Errorf("expected {, got %v", t)
	}
	// read all port entries
	count = 0
	var errs *multierror.Error
	for decoder.More() {
		// read key
		t, err := decoder.Token()
		if err != nil {
			return 0, fmt.Errorf("reading key: %s", err.Error())
		}
		_, ok := t.(string)
		if !ok {
			return 0, fmt.Errorf("expected a key, got: %v", t)
		}
		// decode the value
		var ep EncodedPort
		if err := decoder.Decode(&ep); err != nil {
			errs = multierror.Append(errs, fmt.Errorf("decoding port: %s", err.Error()))
		}
		_, err = portClient.CreatePort(ctx, &portpb.CreatePortRequest{
			Port: ep.toPb(),
		})
		if err != nil {
			errs = multierror.Append(errs, fmt.Errorf("creating port: %s", err.Error()))
		} else {
			count++
		}
	}
	return count, errs.ErrorOrNil()
}
