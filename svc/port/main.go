package main

import (
	"tech.test/m/svc/port/cmd"
)

func main() {
	cmd.Execute()
}
