package server

import (
	"github.com/sirupsen/logrus"
	portpb "tech.test/m/proto/gen/go/ports/v1"
	"tech.test/m/svc/port/pkg/repo"
)

type Server struct {
	*portpb.UnimplementedPortDomainServiceServer
	logger *logrus.Entry
	repo   repo.Repository
}

func New(logger *logrus.Entry, repo repo.Repository) *Server {
	return &Server{
		logger: logger,
		repo:   repo,
	}
}
