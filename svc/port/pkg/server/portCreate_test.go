package server

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/testing/protocmp"
	portpb "tech.test/m/proto/gen/go/ports/v1"
	"tech.test/m/svc/port/pkg/repo/mock"
)

func TestCreatPort(t *testing.T) {
	goldenPort := func() *portpb.Port {
		return &portpb.Port{
			Name:        "test-name",
			Coordinates: []float32{10, 25},
		}
	}
	goldenRequest := func() *portpb.CreatePortRequest {
		return &portpb.CreatePortRequest{
			Port: goldenPort(),
		}
	}
	goldenResponse := func() *portpb.CreatePortResponse {
		return &portpb.CreatePortResponse{
			Port: goldenPort(),
		}
	}
	tts := []struct {
		name string
		repo func(t *testing.T) *mock.MockRepository
		req  *portpb.CreatePortRequest
		want *portpb.CreatePortResponse
		code codes.Code
	}{
		{
			name: "happy path",
			repo: mock.MockRepositoryFactory(
				func(mr *mock.MockRepository) *gomock.Call {
					return mr.EXPECT().CreatePort(context.Background(), goldenPort()).Return(goldenPort(), nil).Times(1)
				},
			),
			req:  goldenRequest(),
			want: goldenResponse(),
			code: codes.OK,
		},
		{
			name: "error - invalid request",
			repo: mock.MockRepositoryFactory(),
			req:  &portpb.CreatePortRequest{},
			want: nil,
			code: codes.InvalidArgument,
		},
		{
			name: "error - database failure",
			repo: mock.MockRepositoryFactory(
				func(mr *mock.MockRepository) *gomock.Call {
					return mr.EXPECT().CreatePort(context.Background(), goldenPort()).Return(nil, errors.New("boom")).Times(1)
				},
			),
			req:  goldenRequest(),
			want: nil,
			code: codes.Internal,
		},
	}
	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			s := &Server{
				repo: tt.repo(t),
			}
			got, err := s.CreatePort(context.Background(), tt.req)
			if errCode := status.Code(err); tt.code != errCode {
				t.Errorf("checking status code: want %s, got %s from error %+v\n", tt.code.String(), errCode.String(), err)
			}
			if diff := cmp.Diff(tt.want, got,
				protocmp.Transform(),
			); diff != "" {
				t.Error(diff)
			}
		})
	}
}
