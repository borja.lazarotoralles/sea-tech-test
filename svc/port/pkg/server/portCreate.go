package server

import (
	"context"
	"errors"

	"github.com/hashicorp/go-multierror"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	portpb "tech.test/m/proto/gen/go/ports/v1"
)

func (s *Server) CreatePort(ctx context.Context, req *portpb.CreatePortRequest) (*portpb.CreatePortResponse, error) {
	if err := validateCreatePortRequest(req); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "validating create port request: %s", err.Error())
	}
	createdPort, err := s.repo.CreatePort(ctx, req.GetPort())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "creating port: %s", err.Error())
	}
	return &portpb.CreatePortResponse{
		Port: createdPort,
	}, nil
}

func validateCreatePortRequest(req *portpb.CreatePortRequest) error {
	var errs *multierror.Error
	port := req.Port
	if port == nil {
		return errors.New("port cannot be nil")
	}
	if port.GetName() == "" {
		errs = multierror.Append(errs, errors.New("port name cannot be blank"))
	}
	if len(port.GetCoordinates()) != 2 {
		errs = multierror.Append(errs, errors.New("port coordinates must be a pair of floats"))
	}
	// TODO: add additional error checking for required properties (not obvious from sample JSON)
	return errs.ErrorOrNil()
}
