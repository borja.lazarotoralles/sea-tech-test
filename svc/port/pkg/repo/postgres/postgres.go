package postgres

import (
	"database/sql"
	"embed"
	"errors"
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
	"tech.test/m/svc/port/pkg/repo"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
)

var _ repo.Repository = &Postgres{}

type Postgres struct {
	db *sql.DB
}

const (
	// name of migrations directory
	migrationsDirectory = "migrations"
)

//go:embed migrations/*.sql
var migrationsFS embed.FS

func New(connectionString string, dbname string) (p *Postgres, closeFunc func() error, err error) {
	source, err := httpfs.New(http.FS(migrationsFS), migrationsDirectory)
	if err != nil {
		return nil, nil, fmt.Errorf("getting migrations source: %s", err.Error())
	}
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, nil, fmt.Errorf("getting sql db instance: %s", err.Error())
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return nil, nil, fmt.Errorf("getting driver instance: %s", err.Error())
	}
	m, err := migrate.NewWithInstance("https", source, dbname, driver)
	if err != nil {
		return nil, nil, fmt.Errorf("getting migrate instance: %s", err.Error())
	}
	if err := m.Up(); err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return nil, nil, fmt.Errorf("migrating up: %s", err.Error())
		}
	}
	return &Postgres{db: db}, func() error { return db.Close() }, nil
}
