CREATE TABLE IF NOT EXISTS port (
  port_name TEXT,
  city TEXT,
  country TEXT,
  alias TEXT[],
  regions TEXT[],
  coordinates DOUBLE PRECISION[],
  province TEXT,
  timezone TEXT,
  unlocs TEXT[],
  code TEXT
);
