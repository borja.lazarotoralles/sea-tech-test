package postgres

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"testing"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
)

const (
	testUsername = "postgres"
	testPassword = "supersecret"
	testHost     = "localhost"
	testPort     = "5432"
	testSSLMode  = "disable"
	testDatabase = "postgres"
)

func testDBPrep(t testing.TB) (p *Postgres, closeFunc func() error) {
	// create db
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
		testUsername,
		testPassword,
		testHost,
		testPort,
		testDatabase,
		testSSLMode,
	)
	p, closeFunc, err := New(connectionString, testDatabase)
	if err != nil {
		t.Fatalf("creating db connection: %s", err.Error())
	}
	return p, closeFunc
}

// testDBMigrations creates db connection, migrates up
func testDBMigrations(t testing.TB, db *sql.DB) (cleanUp func()) {
	source, err := httpfs.New(http.FS(migrationsFS), migrationsDirectory)
	if err != nil {
		t.Fatalf("getting migrations source: %s", err.Error())
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		t.Fatalf("getting driver instance: %s", err.Error())
	}
	m, err := migrate.NewWithInstance("https", source, testDatabase, driver)
	if err != nil {
		t.Fatalf("getting migrate instance: %s", err.Error())
	}
	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		t.Fatalf("migrating up: %s", err.Error())
	}
	return func() {
		if err := m.Down(); err != nil {
			t.Fatalf("migrating down: %s", err.Error())
		}
	}
}
