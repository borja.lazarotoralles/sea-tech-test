package postgres

import (
	"github.com/lib/pq"
	portpb "tech.test/m/proto/gen/go/ports/v1"
)

type repoPort struct {
	name        string
	city        string
	country     string
	alias       pq.StringArray
	regions     pq.StringArray
	coordinates pq.Float32Array
	province    string
	timezone    string
	unlocs      pq.StringArray
	code        string
}

func (rp *repoPort) tpPb() *portpb.Port {
	return &portpb.Port{
		Name:        rp.name,
		City:        rp.city,
		Country:     rp.country,
		Alias:       rp.alias,
		Regions:     rp.regions,
		Coordinates: rp.coordinates,
		Province:    rp.province,
		Timezone:    rp.timezone,
		Unlocs:      rp.unlocs,
		Code:        rp.code,
	}
}
