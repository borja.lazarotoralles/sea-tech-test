package postgres

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"
	"google.golang.org/protobuf/testing/protocmp"
	portpb "tech.test/m/proto/gen/go/ports/v1"
)

func TestCreatePort(t *testing.T) {
	p, close := testDBPrep(t)
	defer close()
	goldenPort := func() *portpb.Port {
		return &portpb.Port{
			Name:        "test-name",
			City:        "test-city",
			Country:     "test-country",
			Alias:       []string{"test-alias-1", "test-alias-2"},
			Regions:     []string{"test-region-1", "test-region-2"},
			Coordinates: []float32{55.5, 25.4},
			Province:    "test-province",
			Timezone:    "test-timezone",
			Unlocs:      []string{"test-unlocs-1", "test-unlocs-2"},
			Code:        "test-code",
		}
	}
	tts := []struct {
		name    string
		in      *portpb.Port
		want    *portpb.Port
		wantErr bool
	}{
		{
			name:    "happy path",
			in:      goldenPort(),
			want:    goldenPort(),
			wantErr: false,
		},
	}
	for _, tt := range tts {
		t.Run(t.Name(), func(t *testing.T) {
			cleanUp := testDBMigrations(t, p.db)
			defer cleanUp()
			got, err := p.CreatePort(context.Background(), tt.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("expectedError is %v but error is %v", tt.wantErr, err)
			}
			if diff := cmp.Diff(tt.want, got,
				protocmp.Transform(),
			); diff != "" {
				t.Error(diff)
			}
		})
	}
}
