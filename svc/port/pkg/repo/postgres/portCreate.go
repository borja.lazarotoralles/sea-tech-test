package postgres

import (
	"context"
	"fmt"

	"github.com/lib/pq"
	portpb "tech.test/m/proto/gen/go/ports/v1"
)

func (p *Postgres) CreatePort(ctx context.Context, in *portpb.Port) (out *portpb.Port, err error) {
	query := `
	INSERT INTO port (port_name, city, country, alias, regions, coordinates, province, timezone, unlocs, code)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	RETURNING port_name, city, country, alias, regions, coordinates, province, timezone, unlocs, code;
	`
	rp := &repoPort{}
	err = p.db.QueryRowContext(ctx, query,
		in.GetName(),
		in.GetCity(),
		in.GetCountry(),
		pq.StringArray(in.GetAlias()),
		pq.StringArray(in.GetRegions()),
		pq.Float32Array(in.GetCoordinates()),
		in.GetProvince(),
		in.GetTimezone(),
		pq.StringArray(in.GetUnlocs()),
		in.GetCode()).
		Scan(&rp.name, &rp.city, &rp.country, &rp.alias, &rp.regions, &rp.coordinates, &rp.province, &rp.timezone, &rp.unlocs, &rp.code)
	if err != nil {
		return nil, fmt.Errorf("inserting port: %s", err.Error())
	}
	return rp.tpPb(), nil
}
