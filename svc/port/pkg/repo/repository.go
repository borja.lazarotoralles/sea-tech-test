package repo

import (
	"context"

	portpb "tech.test/m/proto/gen/go/ports/v1"
)

//go:generate mockgen -source repository.go -destination=./mock/mockRepository.go -package=mock

type Repository interface {
	CreatePort(ctx context.Context, in *portpb.Port) (out *portpb.Port, err error)
}
