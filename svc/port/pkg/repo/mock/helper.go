package mock

import (
	"testing"

	gomock "github.com/golang/mock/gomock"
)

func MockRepositoryFactory(calls ...func(*MockRepository) *gomock.Call) func(t *testing.T) *MockRepository {
	return func(t *testing.T) *MockRepository {
		ctrl := gomock.NewController(t)
		m := NewMockRepository(ctrl)
		for _, call := range calls {
			call(m)
		}
		return m
	}
}
