package mock

import (
	"testing"

	gomock "github.com/golang/mock/gomock"
)

func MockPortDomainClientFactory(calls ...func(*MockPortDomainServiceClient) *gomock.Call) func(t *testing.T) *MockPortDomainServiceClient {
	return func(t *testing.T) *MockPortDomainServiceClient {
		ctrl := gomock.NewController(t)
		m := NewMockPortDomainServiceClient(ctrl)
		for _, call := range calls {
			call(m)
		}
		return m
	}
}
