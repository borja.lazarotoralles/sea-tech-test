# Submission

**Author**: Borja Lazaro Toralles
**Date**: 19/07/2022

# Approach
- The PortDomainService API signature is defined using gRPC endpoints under `proto/idl/...`, using `buf` to generate the Go stubs and `gomock` to generate test mocks.
- The implementation is under `svc/port` with a subdivision of packages:
  - `cmd` contains the root service command, as well as a data ingestion command
  - `config` parses the environment to configure the service/database connections
  - `pkg/server` implements the proto interface
  - `pkg/repo` defines the repository interface and `pkg/repo/mock` contains the matching auto-generated mocks
  - `pkg/repo/postgres` implements the repository interface using Postgres
  - Additionally, the `Dockerfile` specifies how to build the binary in a container, using a multi-stage approach, with the final container having minimal footprint
- The `orchestration` directory contains the `docker-compose.yml` file that sets up the database and services (an example `.env` file is provided for a quick-start)
- Go and proto linters in place

# How to run
1. Place the input JSON file at `data/ports.json` to use to provided Makefile command
2. Start up the database and port services, by running `make reset` (WARNING: running this again will wipe the database)
3. Run `make start_data_ingestion`, to start a data ingestion process that reads from the file and user the service to create port resources
4. Run `make check_table` to check that the database did indeed store all the ports
