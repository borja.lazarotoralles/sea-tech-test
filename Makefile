fmt:
	go fmt ./...

test:
	go test ./...

reset:
	cd orchestration && make down clean up

start_data_ingestion:
	go run svc/port/main.go data-ingestion -f data/ports.json

check_table:
	cd orchestration && make psql DATABASE="port" STATEMENT="SELECT * FROM port;"
